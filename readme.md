# Readme

This is a preprint of the paper titled:

> Increased robustness in reference region model analysis of DCE MRI using two-step constrained approaches  
> Zaki Ahmed & Ives R. Levesque  
> Magnetic Resonance in Medicine. 31 October 2016. [DOI: 10.1002/mrm.26530](https://doi.org/10.1002/mrm.26530)  

The manuscript preprint is available as an:

- [HTML version](https://notzaki.gitlab.io/crrm/)
- [PDF version](https://notzaki.gitlab.io/crrm/_main.pdf)

The general content here should be identical to the published version, but there could be editorial changes in the published version.
These differences will likely be minor, e.g. different word choice or formatting, but if there are any major differences then the published version supersedes the version here.